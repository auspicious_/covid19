import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop:'10px',
    marginBottom:'10px'
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

export default function MainFeaturedPost(props) {
  const classes = useStyles();
  const { report } = props;

  return (
    <div className={classes.root}>
      <Grid
        container
        spacing={3}
        direction="row"
        justify="space-evenly"
        alignItems="center"
      >
        <Grid item xs={6} sm={3}>
          <Paper className={classes.paper}>
            <Typography color="inherit" gutterBottom>
              COVID-19 Dashboard as on :
            </Typography>
            <Typography variant="h6" color="primary">
              {report.date}2020
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className={classes.paper}>
          <Typography color="inherit" gutterBottom>
          Total Active Cases
            </Typography>
          <Typography variant="h6" color="primary">
              {report.totalconfirmed}
            </Typography>
            </Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className={classes.paper}>
          <Typography color="inherit" gutterBottom>
          Total Cured/Discharged
            </Typography>
          <Typography variant="h6" color="primary">
              {report.totalrecovered}
            </Typography>
            </Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className={classes.paper}>
          <Typography color="inherit" gutterBottom>
          Total Death
            </Typography>
            
          <Typography variant="h6" color="primary">
              {report.totaldeceased}
            </Typography>
          </Paper>
         
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className={classes.paper}>
          <Typography color="inherit" gutterBottom>
          Today Confirmed Cases
            </Typography>
          <Typography variant="h6" color="primary">
              {report.dailyconfirmed}
            </Typography>
            </Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className={classes.paper}>
          <Typography color="inherit" gutterBottom>
          Today Death Cases
            </Typography>
            
          <Typography variant="h6" color="primary">
              {report.dailydeceased}
            </Typography>
          </Paper>
         
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className={classes.paper}>
          <Typography color="inherit" gutterBottom>
          Today Recovered Cases
            </Typography>
          <Typography variant="h6" color="primary">
              {report.dailyrecovered}
            </Typography>
            </Paper>
        </Grid>
      </Grid>
    </div>
  );
}

MainFeaturedPost.propTypes = {
  report: PropTypes.object,
};
