import React from "react";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import { makeStyles } from "@material-ui/core/styles";
import { TwitterTimelineEmbed } from "react-twitter-embed";
import Chart from 'react-apexcharts'
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: "10px",
    marginBottom: "10px",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));
export default function Main(props) {
  const classes = useStyles();
  const { options ,series} = props;
console.log("............,,,,,,,,,,,,,,,,,,",props);

  const post = "Demo String";
  return (
    <div className={classes.root}>
      <Grid container spacing={5}>
        <Grid item xs={12} md={7}>
          <Typography variant="h6">Last 10 days record</Typography>
          <Divider />
          {options?<Chart options={options} series={series} type="bar"   width="500" />:null}
        </Grid>
        <Grid item xs={12} md={5}>
          <TwitterTimelineEmbed
            sourceType="profile"
            screenName="MoHFW_INDIA"
            options={{ height: 400 }}
          />
        </Grid>
      </Grid>
    </div>
  );
}

Main.propTypes = {
  title: PropTypes.string,
};
