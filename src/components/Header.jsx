import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import TwitterIcon from "@material-ui/icons/Twitter";
import FacebookIcon from "@material-ui/icons/Facebook";
import Typography from "@material-ui/core/Typography";
import InstagramIcon from "@material-ui/icons/Instagram";
import YouTubeIcon from "@material-ui/icons/YouTube";
import Link from "@material-ui/core/Link";
import {withRouter} from "react-router";
import Avatar from '@material-ui/core/Avatar';
import indianFlag from "../asset/images/indianFlag.png";

const useStyles = makeStyles((theme) => ({
  toolbar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbarSecondary: {
    justifyContent: "space-between",
    overflowX: "auto",
  },
  toolbarLink: {
    padding: theme.spacing(1),
    flexShrink: 0,
  },
  flagImage:{
    display: 'inline-flex',
    paddingTop:'25px'
  }
}));

 function Header(props) {
  const classes = useStyles();
  const { sections, title } = props;

  return (
    <React.Fragment>
      <Toolbar className={classes.toolbar}>
        <Button size="small">Subscribe</Button>
        <Typography
          component="h2"
          variant="h5"
          color="inherit"
          align="center"
          noWrap
          className={classes.toolbarTitle}
        >
          {title}
           <Avatar className={classes.flagImage} alt="India Flag" src={indianFlag} />
        </Typography>
        <Link href="https://twitter.com/mygovindia" target="blank">
          <IconButton>
            <TwitterIcon />
          </IconButton>
        </Link>
        <Link href="https://www.facebook.com/MyGovIndia/" target="blank">
        <IconButton>
          <FacebookIcon />
        </IconButton>
        </Link>
        <Link href="https://www.youtube.com/mygovindia" target="blank">
        <IconButton>
          <YouTubeIcon />
        </IconButton>
        </Link>
        <Link href="https://www.instagram.com/mygovindia/" target="blank">
        <IconButton>
          <InstagramIcon />
        </IconButton>
        </Link>
       
        <Button variant="outlined" size="small" onClick={()=>props.history.push("/login")}>
          Sign in
        </Button>
      </Toolbar>
      <Toolbar
        component="nav"
        variant="dense"
        className={classes.toolbarSecondary}
      >
        {sections.map((section) => (
          <Link
            color="inherit"
            noWrap
            key={section.title}
            variant="h6"
            href={section.url}
            className={classes.toolbarLink}
          >
            {section.title}
          </Link>
        ))}
      </Toolbar>
    </React.Fragment>
  );
}

Header.propTypes = {
  sections: PropTypes.array,
  title: PropTypes.string,
};
export default withRouter(Header);