import { combineReducers } from 'redux';
import covidReducer from "./covidReducer";
const rootReducer = combineReducers({
    covidReducer
});

export default rootReducer;