import {FETCH_DATA,FETCHED_DATA,RECEIVE_ERROR} from "../actions/fetchAction";
  
  const covidReducer = (state = {}, action) => {
    switch (action.type) {
        case FETCH_DATA:
            console.log("fetch");
           return { ...state,isFetching:true};
        case FETCHED_DATA:
            console.log("fetched",action);
           return { ...state,userData: action.data, isFetching: true };
        case RECEIVE_ERROR:
            console.log("fetched",action);
           return { ...state, isError:true };
        default:
           return state;
      }
  };
  
  export default covidReducer;