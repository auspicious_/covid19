import axios from 'axios'
export const FETCH_DATA = 'FETCH_DATA';
export const FETCHED_DATA = 'FETCHED_DATA';
export const RECEIVE_ERROR = 'RECEIVE_ERROR';

export const fetch_covidData = () => {
  return {
    type: FETCH_DATA,
  };
};

export const received_covidData = (post) => {
  return {
    type: FETCHED_DATA,
    data: post,
  };
};

export const receive_error = () => {
  return {
    type: RECEIVE_ERROR,
  };
};

export function  fetchCovidData() {
  return function (dispatch) {
    // dispatch(fetch_covidData());
    return axios.get("https://api.covid19india.org/data.json")
      .then(
        (response) =>  dispatch(received_covidData(response)),
        (error) => console.log("An error occurred.", error)
      )
  };
}
