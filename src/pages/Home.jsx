import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Header from "../components/Header";
import MainFeaturedPost from "../components/MainFeaturedPost";
import FeaturedPost from "../components/FeaturedPost";
import Main from "../components/Main";
import Sidebar from "../components/Sidebar";
import Footer from "../components/Footer";
import { fetchCovidData } from "../actions/fetchAction";
import React, { Component } from "react";
import { connect } from "react-redux";
const useStyles = makeStyles((theme) => ({
  mainGrid: {
    marginTop: theme.spacing(3),
  },
}));

const sections = [
  // { title: 'Technology', url: '#' },
  // { title: 'Design', url: '#' },
  // { title: 'Culture', url: '#' },
  // { title: 'Business', url: '#' },
  // { title: 'Politics', url: '#' },
  // { title: 'Opinion', url: '#' },
  // { title: 'Science', url: '#' },
  // { title: 'Health', url: '#' },
  // { title: 'Style', url: '#' },
  // { title: 'Travel', url: '#' },
  { title: "#IndiaFightsCorona COVID-19", url: "#" },
];

const featuredPosts = [
  {
    title: "Featured post",
    date: "Nov 12",
    description:
      "This is a wider card with supporting text below as a natural lead-in to additional content.",
    image: "https://source.unsplash.com/random",
    imageText: "Image Text",
  },
  {
    title: "Post title",
    date: "Nov 11",
    description:
      "This is a wider card with supporting text below as a natural lead-in to additional content.",
    image: "https://source.unsplash.com/random",
    imageText: "Image Text",
  },
];

const sidebar = {
  title: "About",
  description:
    "Etiam porta sem malesuada magna mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.",
};

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      covidData: "",
      todayCovidData: "",
      lastTenDaysData:""
    };
  }
  componentDidMount = () => {
    this.props.getCovidData();
  };
  componentWillReceiveProps(prevProps) {
    let data = prevProps.covidData.covidReducer.userData.data.cases_time_series;
    let tenDaysData=data.slice(Math.max(data.length - 10, 0));
    let lastTenDate=tenDaysData.map((item)=>{
return item.date       
    })
    let lastTenData=tenDaysData.map((item)=>{
      return item.dailyconfirmed
          })
    let todayData = data[data.length - 1];
    console.log("------------------", lastTenData);
    this.setState({
      covidData: data,
      todayCovidData: todayData,
      options: {
        chart: {
          id: "basic-bar"
        },
        xaxis: {
          categories:lastTenDate 
        }
      },
      series: [
        {
          name: "lastTenDaysSeries",
          data: lastTenData
        }
      ]
    
    });
  }
  render() {
    let classes = useStyles;
    return (
      <React.Fragment>
        <CssBaseline />
        <Container maxWidth="lg">
          <Header title="Covid19 Tracker" sections={sections} />
          <main>
            <MainFeaturedPost report={this.state.todayCovidData} />
           
              <Main title="From the firehose" data={this.state} options={this.state.options} series={this.state.series} />
          
          </main>
    
        </Container>
        <Footer
          title="Footer"
          description="Something here to give the footer a purpose!"
        />
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({ covidData: state });
const mapDispatchToProps = { getCovidData: fetchCovidData };

export default connect(mapStateToProps, mapDispatchToProps)(Home);
